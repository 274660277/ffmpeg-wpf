﻿//using OpenTK.Audio.OpenAL;
//using System;
//using System.Threading.Tasks;

//namespace MediaLib
//{
//    public class AudioCapture : IDisposable
//    {
//        private ALCaptureDevice _captureDevice;

//        /// <summary>
//        /// 麦克风名称
//        /// </summary>
//        public string DeviceName { get; private set; }
//        /// <summary>
//        /// 设备缓冲区大小
//        /// </summary>
//        public int BufferSize { get; private set; }
//        /// <summary>
//        /// 采样率
//        /// </summary>
//        public int Frequency { get; private set; }
//        public bool Recording { get; private set; }
//        private short[] _recordingBuffer;

//        public Action<short[], int> AudioDataHandler { get; set; }

//        /// <summary>
//        /// 初始化麦克风
//        /// </summary>
//        /// <param name="deviceName"></param>
//        /// <param name="freq"></param>
//        /// <param name="format"></param>
//        /// <param name="bufferSize"></param>
//        /// <param name="readBufferSize"></param>
//        public bool InitCapture(string deviceName, int freq = 8000, ALFormat format = ALFormat.Mono16, int bufferSize = 4096, int readBufferSize = 1024)
//        {
//            DeviceName = deviceName;
//            Frequency = freq;
//            BufferSize = bufferSize;
//            _captureDevice = ALC.CaptureOpenDevice(deviceName, freq, format, bufferSize);
//            ALError err = AL.GetError();
//            return (_captureDevice.Handle.ToInt64() != 0) && (err == ALError.NoError);
//        }

//        public void StartCaptureAudio()
//        {
//            if (Recording) { return; }
//            _recordingBuffer = new short[BufferSize];
//            ALC.CaptureStart(_captureDevice);
//            ALError err = AL.GetError();
//            AL.DistanceModel(ALDistanceModel.None);
//            Recording = true;
//            if (err == ALError.NoError)
//            {
//                Task.Factory.StartNew(CaptureAudioToBuffer);
//            }
//        }

//        public void StopCaptureAudio()
//        {
//            if (!Recording) { return; }
//            ALC.CaptureStop(_captureDevice);
//            Recording = false;
//        }

//        private void CaptureAudioToBuffer()
//        {
//            if (!Recording) { return; }
//            while (Recording)
//            {
//                int sample = ALC.GetAvailableSamples(_captureDevice);
//                if (sample >= BufferSize)
//                {
//                    ALC.CaptureSamples<short>(_captureDevice, _recordingBuffer, sample);
//                    AudioDataHandler?.Invoke(_recordingBuffer, sample);
//                }
//            }
//        }

//        public void SaveAudioHandler(byte[] data, int size)
//        {
//            //OpenTK.Audio.OpenAL
//            Console.WriteLine($"{DateTime.Now:HHmmss.fff}:{size}");
//        }


//        #region dispose
//        private bool disposed;

//        protected virtual void Dispose(bool disposing)
//        {
//            if (!disposed)
//            {
//                if (disposing)
//                {
//                    ALC.CaptureCloseDevice(_captureDevice);

//                    // TODO: 释放托管状态(托管对象)
//                }

//                // TODO: 释放未托管的资源(未托管的对象)并重写终结器
//                // TODO: 将大型字段设置为 null
//                disposed = true;
//            }
//        }

//        // // TODO: 仅当“Dispose(bool disposing)”拥有用于释放未托管资源的代码时才替代终结器
//        // ~AudioCapture()
//        // {
//        //     // 不要更改此代码。请将清理代码放入“Dispose(bool disposing)”方法中
//        //     Dispose(disposing: false);
//        // }

//        public void Dispose()
//        {
//            // 不要更改此代码。请将清理代码放入“Dispose(bool disposing)”方法中
//            Dispose(disposing: true);
//            GC.SuppressFinalize(this);
//        }
//        #endregion

//    }
//}