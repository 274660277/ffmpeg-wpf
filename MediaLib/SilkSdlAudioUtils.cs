﻿using Silk.NET.SDL;
using System;

namespace MediaLib;

public sealed class SilkSdlAudioUtils : IDisposable
{
    private Sdl _sdl;

    public SilkSdlAudioUtils()
    {
        _sdl = Sdl.GetApi();
        _sdl.Init(Sdl.InitAudio);
        //_sdl.AudioStreamClear
    }


    #region Dispose
    private bool disposedValue;

    protected void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                // TODO: 释放托管状态(托管对象)
            }

            disposedValue = true;
        }
    }

    public void Dispose()
    {
        // 不要更改此代码。请将清理代码放入“Dispose(bool disposing)”方法中
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
    #endregion
}