﻿//using NAudio.CoreAudioApi;
//using System.Collections.Generic;
//using System.Linq;

//namespace MediaLib
//{
//    /// <summary>
//    /// 音频设备管理
//    /// </summary>
//    public sealed class AudioDevice
//    {
//        /// <summary>
//        /// 查询本地可用麦克风
//        /// </summary>
//        /// <returns></returns>
//        public static List<MMDevice> GetAudioCaptureDevice()
//        {
//            using MMDeviceEnumerator deviceEnumerator = new MMDeviceEnumerator();
//            MMDeviceCollection mDevices = deviceEnumerator.EnumerateAudioEndPoints(DataFlow.Capture, DeviceState.Active);
//            return mDevices.ToList();
//        }

//        /// <summary>
//        /// 查询本地音频播放器
//        /// </summary>
//        /// <returns></returns>
//        public static List<MMDevice> GetAudioPlayerDevice()
//        {
//            using MMDeviceEnumerator deviceEnumerator = new MMDeviceEnumerator();
//            MMDeviceCollection mDevices = deviceEnumerator.EnumerateAudioEndPoints(DataFlow.Render, DeviceState.Active);
//            return mDevices.ToList();
//        }
//    }
//}