﻿using System;

namespace MediaLib.Interface;

public interface IAudioPlayer : IDisposable
{
    string AudioFile { get; }
    int Size { get; }
    long Bitrate { get; }
    int ChannelNum { get; }
    int SampleRate { get; }

    void Play();

    void Pause(bool pause);

    void Stop();

    void AdjustVolume(float vol);

    void AdjustSpeed(float vol);

    /// <summary>
    /// 获取文件信息
    /// </summary>
    /// <param name="audioFile"></param>
    /// <param name="bitrate">比特率</param>
    /// <param name="sampleRate">采样率</param>
    /// <param name="channelNum">通道数</param>
    /// <param name="depthBit">位深</param>
    /// <param name="size">文件大小(Byte)</param>
    void GetAudioInfo(string audioFile, out long bitrate, out int sampleRate, out int channelNum, out int depthBit, out int size);
}