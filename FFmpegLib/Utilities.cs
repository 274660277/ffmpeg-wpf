﻿using System;

namespace FFmpegLib
{
    public sealed class Utilities
    {
        public static unsafe string PtrToStringUTF8(byte* stringAddress)
        {
            if (stringAddress == null) return null;
            if (*stringAddress == 0) return string.Empty;

            return System.Runtime.InteropServices.Marshal.PtrToStringUTF8(new IntPtr(stringAddress));

            //int byteLength = 0;
            //while (true)
            //{
            //    if (stringAddress[byteLength] == 0)
            //        break;
            //    byteLength++;
            //}
            //byte* stringBuffer = stackalloc byte[byteLength];
            //Buffer.MemoryCopy(stringAddress, stringBuffer, byteLength, byteLength);
            //return Encoding.UTF8.GetString(stringBuffer, byteLength);
        }

    }
}