﻿using FFmpeg.AutoGen;
using System;

namespace FFmpegLib
{
    public sealed unsafe class FAVPacket : IDisposable
    {
        private AVPacket* _ptr;
        public AVPacket* Ptr { get => _ptr; }
        public AVMediaType MediaType = AVMediaType.AVMEDIA_TYPE_UNKNOWN;

        private int _streamIndex = -1;
        public int StreamIndex
        {
            get { return _ptr->stream_index; }
            private set { _streamIndex = value; }
        }

        public FAVPacket()
        {
            _ptr = ffmpeg.av_packet_alloc();
        }

        public FAVPacket(AVPacket* pkt)
        {
            _ptr = pkt;
        }

        public FAVPacket(IntPtr* ptr)
        {
            _ptr = (AVPacket*)ptr;
        }

        public void UnRef()
        {
            if (_ptr != null)
                ffmpeg.av_packet_unref(_ptr);
        }

        public void Ref(AVPacket* src)
        {
            if (_ptr != null)
                ffmpeg.av_packet_ref(_ptr, src);
        }

        #region dispose
        private bool disposedValue;
        protected void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    UnRef();
                    fixed (AVPacket** ptr = &_ptr)
                        ffmpeg.av_packet_free(ptr);
                    _ptr = null;
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // 不要更改此代码。请将清理代码放入“Dispose(bool disposing)”方法中
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}
