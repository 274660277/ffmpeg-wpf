﻿
namespace GLWinForm
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.glCtrlView = new SharpGL.OpenGLControl();
            ((System.ComponentModel.ISupportInitialize)(this.glCtrlView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(140, 657);
            this.panel1.TabIndex = 0;
            // 
            // glCtrlView
            // 
            this.glCtrlView.DrawFPS = false;
            this.glCtrlView.Location = new System.Drawing.Point(169, 12);
            this.glCtrlView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.glCtrlView.Name = "glCtrlView";
            this.glCtrlView.OpenGLVersion = SharpGL.Version.OpenGLVersion.OpenGL2_1;
            this.glCtrlView.RenderContextType = SharpGL.RenderContextType.DIBSection;
            this.glCtrlView.RenderTrigger = SharpGL.RenderTrigger.TimerBased;
            this.glCtrlView.Size = new System.Drawing.Size(1002, 657);
            this.glCtrlView.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 681);
            this.Controls.Add(this.glCtrlView);
            this.Controls.Add(this.panel1);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.glCtrlView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private SharpGL.OpenGLControl glCtrlView;
    }
}

