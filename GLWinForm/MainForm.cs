﻿using SharpGL;
using SharpGL.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GLWinForm
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            OpenGL gl = glCtrlView.OpenGL;

            //GLLoadImg();
            
        }

        private void GLLoadImg(OpenGL gl, int width, int height, IntPtr ptr)
        {
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.TexImage2D(
                OpenGL.GL_TEXTURE_2D,
                0,
                OpenGL.GL_RGB16,
                width,
                height,
                0,
                OpenGL.GL_RGBA,
                OpenGL.GL_BYTE,
                ptr
                );
            gl.End();
        }
    }
}
