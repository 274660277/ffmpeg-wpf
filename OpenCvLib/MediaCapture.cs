﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenCvLib
{
    public class MediaCapture
    {
        private OpenCvSharp.VideoCapture _capture;

        public double FrameWidth { get; private set; }
        public double FrameHeight { get; private set; }
        public double Fps { get; private set; }
        public OpenCvSharp.CaptureType Type { get; private set; }


        public MediaCapture()
        {
            _capture = new OpenCvSharp.VideoCapture();
        }

        public bool OpenCamera(int cameraIndex = 0)
        {
            bool openRs = _capture.Open(cameraIndex);
            if (!openRs) { return false; }
            Type = _capture.CaptureType;
            Fps = _capture.Fps;
            FrameWidth = _capture.Get(OpenCvSharp.VideoCaptureProperties.FrameWidth);
            FrameHeight = _capture.Get(OpenCvSharp.VideoCaptureProperties.FrameHeight);
            return openRs;
        }

        public bool OpenVideo(string source)
        {
            bool openRs = _capture.Open(source);
            if (!openRs) { return false; }
            Type = _capture.CaptureType;
            Fps = _capture.Fps;
            FrameWidth = _capture.Get(OpenCvSharp.VideoCaptureProperties.FrameWidth);
            FrameHeight = _capture.Get(OpenCvSharp.VideoCaptureProperties.FrameHeight);
            return openRs;
        }

        public OpenCvSharp.Mat ReadFrame()
        {
            OpenCvSharp.Mat mat = new OpenCvSharp.Mat();
            bool readRs = _capture.Read(mat);
            if (readRs)
            {
                return mat;
            }
            mat.Dispose();
            return null;
        }
    }
}