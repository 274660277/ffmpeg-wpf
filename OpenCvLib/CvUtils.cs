﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenCvLib
{
    public class CvUtils
    {
        /// <summary>
        /// 转二值图
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        public static Mat ToBinaryGraph(Mat src)
        {
            //src.
            // 灰度图
            using Mat dst = src.CvtColor(ColorConversionCodes.RGB2GRAY);
            int type = (int)ThresholdTypes.Otsu | (int)ThresholdTypes.Binary;
            return dst.Threshold(0, 255, (ThresholdTypes)type);
        }
    }
}