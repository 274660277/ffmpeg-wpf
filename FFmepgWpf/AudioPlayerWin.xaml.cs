﻿using MediaLib.Implement;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FFmepgWpf
{
    /// <summary>
    /// AudioPlayerWin.xaml 的交互逻辑
    /// </summary>
    public partial class AudioPlayerWin : MahApps.Metro.Controls.MetroWindow
    {
        //private List<AudioPlayer> _players;
        SilkAudioPlayer player;

        private double curSeconds;

        public AudioPlayerWin()
        {
            InitializeComponent();
            //_players = new List<AudioPlayer>();
            //System.Windows.Controls
        }

        private void Display(FFmpegLib.FAVStream info)
        {
            string secStr = info.TotalSecs.ToString();
            int pos = secStr.IndexOf(".");
            if (pos > -1)
                secStr = secStr.Substring(0, pos + 2);
            Dispatcher.Invoke(() =>
            {
                curTimespan.Text = info.GetCurMsStr();
                progressStateSlider.Value = curSeconds;
                progressStateSlider.Maximum = info.TotalSecs;
                totalSpan.Text = secStr;
            });
        }

        private void AudioFileSelectBtnClick(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            dialog.Filter = "音频文件|*.mp3;*.wav;*.flac";
            bool? openRs = dialog.ShowDialog();
            if (openRs.HasValue && openRs.Value)
            {
                fileTBlock.Text = dialog.FileName;
            }
        }

        private void PlayAudioBtnClick(object sender, RoutedEventArgs e)
        {
            if (player == null)
            {
                string audio = fileTBlock.Text;
                if (!File.Exists(audio))
                {
                    MessageBox.Show("文件不存在", "提示:", MessageBoxButton.OK);
                    return;
                }
                //AudioPlayer player = new AudioPlayer(audio);
                //_players.Add(player);
                //player.Play();
                player = new SilkAudioPlayer(audio);
                player.DisplayInfo += Display;
                player.Play();
            }
            player.CurCtlStatus = Silk.NET.OpenAL.SourceState.Playing;
            PlayBtnStatus(player.CurCtlStatus);
        }

        private void WinClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (player != null)
            {
                player.DisplayInfo -= Display;
                player.Dispose();
                player = null;
            }
        }

        private void PauseAudioBtnClick(object sender, RoutedEventArgs e)
        {
            if (player == null) { return; }
            player.CurCtlStatus = Silk.NET.OpenAL.SourceState.Paused;
            PlayBtnStatus(player.CurCtlStatus);
        }

        private void EndAudioBtnClick(object sender, RoutedEventArgs e)
        {
            if (player == null) { return; }
            player.CurCtlStatus = Silk.NET.OpenAL.SourceState.Stopped;
            PlayBtnStatus(player.CurCtlStatus);
            player.Stop();
            player.DisplayInfo -= Display;
            player.Dispose();
            player = null;
            totalSpan.Text = "00:00:00";
        }

        private void PlayBtnStatus(Silk.NET.OpenAL.SourceState status)
        {
            if (status == Silk.NET.OpenAL.SourceState.Initial
                || status == Silk.NET.OpenAL.SourceState.Stopped)
            {
                audioFileSelectBtn.IsEnabled = true;
                playAudioBtn.IsEnabled = true;
                pauseAudioBtn.IsEnabled = false;
                endAudioBtn.IsEnabled = false;
            }
            else if (status == Silk.NET.OpenAL.SourceState.Playing)
            {
                audioFileSelectBtn.IsEnabled = false;
                playAudioBtn.IsEnabled = false;
                pauseAudioBtn.IsEnabled = true;
                endAudioBtn.IsEnabled = true;
            }
            else if (status == Silk.NET.OpenAL.SourceState.Paused)
            {
                audioFileSelectBtn.IsEnabled = false;
                playAudioBtn.IsEnabled = true;
                pauseAudioBtn.IsEnabled = false;
                endAudioBtn.IsEnabled = true;
            }

        }

        /// <summary>
        /// 播放进度
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProgressStateSliderValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Slider obj = sender as Slider;
            curSeconds = obj.Value;
        }

        private void PsSliderGotMouseCapture(object sender, MouseEventArgs e)
        {
            //if (player.CurCtlStatus == Silk.NET.OpenAL.SourceState.Playing)
            //    player?.Pause(true);
        }

        private void PsSliderLostMouseCapture(object sender, MouseEventArgs e)
        {
            if (player.CurCtlStatus == Silk.NET.OpenAL.SourceState.Playing)
                player.Seek(curSeconds);
            Console.WriteLine(curSeconds);
        }

    }
}