﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FFmepgWpf
{
    /// <summary>
    /// MediaPlayerWin.xaml 的交互逻辑
    /// </summary>
    public partial class MediaPlayerWin : Window
    {
        public MediaPlayerWin()
        {
            InitializeComponent();
        }

        private void MediaFileSelectBtnClick(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            dialog.Filter = "媒体文件|*.mp4;*.avi;*.rmvb";
            bool? openRs = dialog.ShowDialog();
            if (openRs.HasValue && openRs.Value)
            {
                fileTBlock.Text = dialog.FileName;
            }
        }

        private void PlayBtnClick(object sender, RoutedEventArgs e)
        {

        }

        //private void OpenGLControl_OpenGLDraw(object sender, SharpGL.WPF.OpenGLRoutedEventArgs args)
        //{
        //    OpenGL gl = args.OpenGL;
        //    SharpGL.SceneGraph.Assets.Texture texture = new SharpGL.SceneGraph.Assets.Texture();
        //    //texture.Create(gl,);
        //}

        //private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.WPF.OpenGLRoutedEventArgs args)
        //{
        //}

        //private void OpenGLControl_Resized(object sender, SharpGL.WPF.OpenGLRoutedEventArgs args)
        //{
        //}
    }
}