using Avalonia.Controls;
using Avalonia.Interactivity;

namespace AvaloniaApp.Views;

public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();
    }

    private void AudioPlayerClick(object sender, RoutedEventArgs args) { }

    private void MediaPlayerClick(object sender, RoutedEventArgs args) { }

    private void OpenGLMediaPlayerClick(object sender, RoutedEventArgs args) { }
}