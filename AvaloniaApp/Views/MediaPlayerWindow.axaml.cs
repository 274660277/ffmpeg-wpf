using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Avalonia.Platform.Storage;

namespace AvaloniaApp;

public partial class MediaPlayerWindow : Window
{
    private static FilePickerFileType MediaFilesType { get; } = new("All Images")
    {
        Patterns = new[] { "*.mp4", "*.avi", "*.rmvb" },
        AppleUniformTypeIdentifiers = new[] { "public.media" },
        //MimeTypes = new[] { "image/*" }
    };

    public MediaPlayerWindow()
    {
        InitializeComponent();
        //mediaFileSelectBtn.Click += MediaFileSelectBtnClick;
    }

    public async void MediaFileSelectBtnClick(object sender, RoutedEventArgs e)
    {
        var fileList = await StorageProvider.OpenFilePickerAsync(new FilePickerOpenOptions()
        {
            Title = "选择媒体文件",
            FileTypeFilter = new[] { MediaFilesType }
        });

        //Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
        //dialog.Filter = "媒体文件|*.mp4;*.avi;*.rmvb";
        //bool? openRs = dialog.ShowDialog();
        //if (openRs.HasValue && openRs.Value)
        //{
        //    fileTBlock.Text = dialog.FileName;
        //}
    }

    private void PlayBtnClick(object sender, RoutedEventArgs e)
    {

    }
}