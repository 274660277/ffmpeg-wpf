﻿namespace AvaloniaApp.ViewModels
{
    public partial class MainWindowViewModel : ViewModelBase
    {
        public string Title => "主窗体";

        public string Greeting => "Welcome to Avalonia!";
    }
}
